<?php

namespace App\Http\Livewire\Admin\Profile;

use App\Models\Admins;
use Illuminate\Support\Facades\Hash;
use Livewire\Component;

class Password extends Component
{
    public $password, $confirmation;

    protected $rules = [
        'password'  => 'required',
        'confirmation'  => 'required'
    ];
    protected $messages = [
        'password.required' => 'Oops, password tidak boleh kosong!',
        'confirmation.required' => 'Oops, konfirmasi password tidak boleh kosong!',
    ];

    public function updated()
    {
        $this->validate();
    }

    public function show(){
        $this->password = '';
        $this->confirmation = '';
        $this->dispatchBrowserEvent('pModalShow');
    }

    public function setup(){
        $id = auth('admin')->user()->id_admins;
        if($this->password != $this->confirmation){
            $this->dispatchBrowserEvent('error', 'Oops, password dan konfrimasi password tidak sama!');
        } else {
            $data = Admins::find($id);
            $data->password = Hash::make($this->password);
            if($data->save()){
                $this->dispatchBrowserEvent('success', 'Password berhasil dirubah!');
            }else{
                $this->dispatchBrowserEvent('error', 'Oops, maaf database sedang sibuk!');
            }
            $this->dispatchBrowserEvent('pModalExpand');   
        }
    }

    public function render()
    {
        return view('livewire.admin.profile.password');
    }
}
