<?php

namespace App\Http\Livewire\Admin\Cash;

use App\Models\cash;
use Livewire\Component;

class Counter extends Component
{
    public function render()
    {
        $price = cash::where('status', 'success')->sum('price');
        $pending = cash::where('status', 'pending')->sum('price');
        $tf = cash::where('method', 'transfer')->sum('price');
        $case = cash::where('method', 'case')->sum('price');
        return view('livewire.admin.cash.counter', [
            'price' => $price,
            'pending' => $pending,
            'tf' => $tf,
            'case' => $case,
        ]);
    }
}
