<?php

namespace App\Http\Livewire\Admin\Cash;

use App\Models\cash;
use Livewire\Component;
use Livewire\WithFileUploads;

class Data extends Component
{
  use WithFileUploads;
  public $id_cash;
  public $username, $price, $method, $status, $dates, $images;
  public $edit = false;

  public $month, $year;

  protected $listeners = ['deleteAction' => 'delete'];

  protected $rules = [
    'username' => 'required|max:100',
    'price' => 'required',
    'method' => 'required',
    'dates' => 'required',
    'images'  => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:4096',
  ];

  protected $messages = [
    'username.required' => 'Nama keluarganye masih kosong tolol!',
    'price.required' => 'Masukin jumlah pembayaranye tolol!',
    'method.required' => 'Goblok, pilih metode pembayarannye!',
    'dates.required' => 'Tolol, Tanggal tidak boleh kosong!',
    'images.required' => 'Foto buktinye diaplod goblok!',
    'images.image' => 'Tolol itu bukan gambar goblok!',
    'images.mimes' => 'Format gambar lo harus jpeg, png, jpg atau svg!',
    'images.max' => 'Ukuran gambar maksimal 4mb goblok!',
  ];

  public function clearInput()
  {
    $this->username = '';
    $this->dates = '';
    $this->price = '';
    $this->method = '';
    $this->status = '';
    $this->images = '';
  }

  public function show()
  {
    if($this->edit = true){
        $this->edit = false;
    }
    $this->clearInput();
    $this->dispatchBrowserEvent('showModal');
  }

  public function store()
  {
    $this->validate();

    // images 
    $resorce = $this->images;
    $originNamaImages = $resorce->getClientOriginalName();
    $NewNameImage = "IMG-" . substr(md5($originNamaImages . date("YmdHis")), 0, 14);
    $images = $NewNameImage . "." . $resorce->getClientOriginalExtension();

    $data = new cash();
    $data->username = $this->username;
    $data->date = $this->dates;
    $data->status = 'success';
    $data->price = $this->price;
    $data->method = $this->method;
    $data->images = $images;
    if ($data->save()) {
      $resorce->storeAs('/images/cash/',  $images, 'myLocal');
      $data->invoice = 'INV/' . strtoupper(substr(md5($data->id_cash), 0, 6)) . '/NENG/' . date('His');
      $data->save();
      $this->dispatchBrowserEvent('success', 'Dana kas berhasil ditambahkan');
    } else {
      $this->dispatchBrowserEvent('error', 'Oops, Maaf terjadi kesalahan data!');
    }
    $this->dispatchBrowserEvent('expandModal');
  }

  public function edit($id)
  {
    $data = cash::find($id);
    $this->username = $data->username;
    $this->price = $data->price;
    $this->status = $data->status;
    $this->method = $data->method;
    $this->dates = $data->date;
    $this->images = $data->images;
    $this->id_cash = $data->id_cash;
    $this->edit = true;
    $this->dispatchBrowserEvent('showModal');
  }
  public function update()
  {
    $data = cash::find($this->id_cash);
    $data->date = $this->dates;
    $data->status = $this->status;
    if ($data->save()) {
      $this->dispatchBrowserEvent('success', 'Dana kas berhasil dirubah');
    } else {
      $this->dispatchBrowserEvent('error', 'Oops, Maaf terjadi kesalahan data!');
    }
    $this->clearInput();
    $this->dispatchBrowserEvent('expandModal');
    $this->edit = false;
  }

  public function removed($id)
  {
    $this->id_cash = $id;
    $this->dispatchBrowserEvent('deleteConfrimed');
  }

  public function delete()
  {
    $data = cash::find($this->id_cash);
    if ($data) {
      $data->delete();
      $this->dispatchBrowserEvent('success', 'Dana kas telah tehapus!');
    } else {
      $this->dispatchBrowserEvent('error', 'Oops, Maaf terjadi kesalahan data!');
    }
  }

  public function mount()
  {
    $this->month = date('m');
    $this->year = date('Y');
  }

  public function render()
  {
    $data = cash::orderBy('date', 'ASC')->whereMonth('date', $this->month)->whereYear('date', $this->year)->get();
    return view('livewire.admin.cash.data', ['data' => $data]);
  }
}
