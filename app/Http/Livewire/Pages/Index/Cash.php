<?php

namespace App\Http\Livewire\Pages\Index;

use App\Models\cash as ModelsCash;
use Livewire\Component;

class Cash extends Component
{
    public $id_cash;
    public $images;
    public $month, $years;
    public $name;

    public $dates = ['januari', 'febuari', 'maret', 'april', 'mei', 'juni', 'juli', 'agustus', 'november', 'september', 'oktober', 'desember'];

    public function mount(){
        // $this->month = 1;
        $this->years = date('Y');
        $this->month = intval(date('m')) ;
        // dd($this->month);
    }

    public function show($id)
    {
        $data = ModelsCash::find($id);
        $this->images = $data->images;
        $this->dispatchBrowserEvent('showModals');
    }

    public function render()
    {
        if($this->name){
            $data = ModelsCash::where('username', 'like', '%' . $this->name . '%')->get();
        } else {
            $data = ModelsCash::whereYear('date', $this->years)->whereMonth('date', $this->month)->get();
        }
        return view('livewire.pages.index.cash', ['data' => $data]);
    }
}
