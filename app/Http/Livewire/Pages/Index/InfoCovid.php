<?php

namespace App\Http\Livewire\Pages\Index;

use Livewire\Component;

class InfoCovid extends Component
{
    public function render()
    {
        return view('livewire.pages.index.info-covid');
    }
}
