<?php

namespace App\Http\Livewire\Pages\Index;

use App\Models\cash;
use Livewire\Component;

class InfoCash extends Component
{
    public function render()
    {
        $cash = cash::sum('price');
        $cashPending = cash::where('status', 'pending')->sum('price');
        $buyerCount = cash::count();
        return view('livewire.pages.index.info-cash', [
            'cash' => $cash,
            'buyerCount' => $buyerCount,
            'cashPending' => $cashPending,
        ]);
    }
}
