<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class cash extends Model
{
    use HasFactory;

    protected $table = 'cash';

    protected $primaryKey = 'id_cash';

    protected $fillable = [
        'invoice',
        'username',
        'status',
        'price',
        'method',
        'date',
        'images',
        'user_id',
    ];
}
