<?php

use App\Http\Controllers\admin\bannerAdmin;
use App\Http\Controllers\admin\cashAdmin;
use App\Http\Controllers\admin\indexAdmin;
use App\Http\Controllers\admin\newsAdmin;
use App\Http\Controllers\admin\profileAdmin;
use App\Http\Controllers\auth\adminAuth;
use App\Http\Controllers\pages\indexController;
use App\Http\Controllers\pages\newsController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', [indexController::class, 'index'])->name('index');
Route::get('/beranda', [indexController::class, 'index'])->name('index');
Route::get('/beranda/berita', [newsController::class, 'index'])->name('news');
Route::get('/beranda/berita/{slug}', [newsController::class, 'detail'])->name('news.detail');
Route::get('/beranda/tentang-kami', [indexController::class, 'about'])->name('about');


Route::get('/admin/login', [adminAuth::class, 'login'])->name('admin.login');
Route::post('/admin/login/post', [adminAuth::class, 'loginPost'])->name('admin.login.store');
Route::get('/admin/register', [adminAuth::class, 'register'])->name('admin.register');
Route::group(['prefix' => 'admin', 'middleware' => 'auth:admin'], function () {
    Route::get('/', [indexAdmin::class, 'index'])->name('admin.index');
    Route::get('/dashboard', [indexAdmin::class, 'index'])->name('admin.index.dashboard');
    // profile admin routing
    Route::get('/profile', [profileAdmin::class, 'index'])->name('admin.profile');
    // cash routing
    Route::get('/cash', [cashAdmin::class, 'index'])->name('admin.cash');
    // Routing news
    Route::get('/news', [newsAdmin::class, 'index'])->name('admin.news');
    Route::get('/news/create', [newsAdmin::class, 'create'])->name('admin.news.create');
    Route::post('/news/create/store', [newsAdmin::class, 'store'])->name('admin.news.create.store');
    Route::get('/news/edit/{id}', [newsAdmin::class, 'edit'])->name('admin.news.edit');
    Route::put('/news/update/{id}', [newsAdmin::class, 'update'])->name('admin.news.update');
    Route::get('/news/upload/editore', [newsAdmin::class, 'editor'])->name('admin.news.upload.editor');

     // banners routing admin
     Route::get('/banners', [bannerAdmin::class, 'index'])->name('admin.banners');
     Route::get('/banners/create', [bannerAdmin::class, 'create'])->name('admin.banners.create');
     

    Route::get('/logout', [indexAdmin::class, 'logout'])->name('admin.logout');
});
