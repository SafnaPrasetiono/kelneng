<div>
    <div class="d-flex p-3 border-bottom">
        <button wire:click='show' type="button" class="btn btn-outline-secondary me-1">Tambah</button>
        <div class="d-flex ms-auto">
            <select wire:model='month' class="form-select">
                <?php
                $bulan = ["Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember"];
                ?>
                @foreach ($bulan as $index => $m)
                <option value="0{{ $index + 1 }}">
                    {{ $m }}
                </option>
                @endforeach
            </select>
            <select wire:model='year' class="form-select ms-1">
                @for ($i = date('Y'); $i >= 2021; $i--)
                <option value="{{ $i }}">
                    {{ $i }}
                </option>
                @endfor
            </select>
        </div>
    </div>
    <div class="d-block p-3">
        <div class="table-responsive">
            <table class="table table-borderless">
                <thead class="alert-secondary">
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Invoice</th>
                        <th scope="col">Username</th>
                        <th scope="col">Pembayaran</th>
                        <th scope="col">Status</th>
                        <th scope="col">Date</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($data as $index => $item)
                    <tr>
                        <th scope="row">{{ $index + 1 }}</th>
                        <td>{{ $item->invoice }}</td>
                        <td class="text-capitalize text-nowrap">{{ $item->username }}</td>
                        <td class="text-capitalize">{{ $item->method }}</td>
                        <td>
                            @if($item->status == "success")
                            <span class="badge text-bg-success rounded-pill px-3">success</span>
                            @else
                            <span class="badge text-bg-secondary rounded-pill px-3">pending</span>
                            @endif
                        </td>
                        <td>
                            <div class="text-nowrap">{{ date("d F Y", strtotime($item->date))}}</div>
                        </td>
                        <td class="text-nowrap">
                            <button wire:click="view({{ $item->id_cash }})" type="button"
                                class="btn btn-outline-secondary btn-sm">
                                <i class="fas fa-eye fa-sm fa-fw"></i>
                            </button>
                            <button wire:click="edit({{ $item->id_cash }})" class="btn btn-outline-secondary btn-sm">
                                <i class="fas fa-pencil-alt fa-sm fa-fw"></i>
                            </button>
                            <button wire:click="removed({{ $item->id_cash }})" type="button"
                                class="btn btn-outline-secondary btn-sm">
                                <i class="fas fa-trash fa-sm fa-fw"></i>
                            </button>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <div class="d-flex align-items-center p-3">
        <p class="mb-0 border py-1 px-2 rounded">
            <span class="fw-bold">Data : {{ $data->count() }}</span>
        </p>
        {{-- @if ($data->hasPages())
        <nav class="ms-auto">
            {{ $data->links('components.paginations') }}
        </nav>
        @endif --}}
    </div>

    <div wire:ignore.self class="modal fade" tabindex="-1" id="NewModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Tambah Kas</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="mb-3 placeholder-glow">
                        <label for="kel" class="form-label">Nama keluarga</label>
                        <input wire:model='username' type="text"
                            class="form-control @error('username') is-invalid @enderror" id="kel"
                            wire:loading.class='placeholder' wire:target='store' wire:target='update' @if ($edit == true) disabled @endif>
                        @error('username')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>
                    <div class="mb-3 placeholder-glow">
                        <label for="dates" class="form-label">Tanggal</label>
                        <input wire:model='dates' type="date"
                            class="form-control @error('dates') is-invalid @enderror" id="dates"
                            wire:loading.class='placeholder' wire:target='store, update'>
                        @error('dates')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>
                    <div class="mb-3 placeholder-glow">
                        <label for="method" class="form-label">Metode Pembayaran</label>
                        <select wire:model='method' name="method" id="method"
                            class="form-select @error('method') is-invalid @enderror" wire:loading.class='placeholder'
                            wire:target='store' wire:target='update'  @if ($edit == true) disabled @endif>
                            <option value="" selected>Pilih metode pembayaran</option>
                            <option value="case">Case</option>
                            <option value="transfer">Transfer</option>
                        </select>
                        @error('method')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>
                    <div class="mb-3 placeholder-glow">
                        <label for="price" class="form-label">Total harga</label>
                        <input wire:model='price' type="number"
                            class="form-control @error('price') is-invalid @enderror" id="price"
                            wire:loading.class='placeholder' wire:target='store' wire:target='update'  @if ($edit == true) disabled @endif>
                        @error('price')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>
                    <div class="mb-3 placeholder-glow">
                        <label for="upload" class="d-flex align-items-center justify-content-center rounded mb-2"
                            style="min-height: 200px; border:2px dashed @error('images') #ff000040 @else #00000070 @enderror;"
                            wire:loading.class='placeholder' wire:target='store' wire:target='update'>
                            @if ($images)
                            @if ($edit == true)
                            <img src="{{ url('/images/cash/' . $images) }}" class="img-fluid">
                            @else
                            <img src="{{ $images->temporaryUrl() }}" class="img-fluid">
                            @endif
                            @else
                            <div class="text-center text-secondary">
                                <i class="fas fa-upload fa-2x fa-fw mb-2"></i>
                                <small class="d-block">Upload File</small>
                            </div>
                            @endif
                        </label>
                        <input wire:model='images' type="file" name="images" id="upload"
                            class="form-control @error('images') is-invalid @enderror" wire:loading.class='placeholder'
                            wire:target='store' wire:target='update'  @if ($edit == true) disabled @endif>
                        @error('images')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>
                    @if ($edit == true)
                    <div class="mb-3 placeholder-glow">
                        <label for="status" class="form-label">Status pembayaran</label>
                        <select wire:model='status' name="status" id="status"
                            class="form-select @error('status') is-invalid @enderror" wire:loading.class='placeholder'
                            wire:target='store' wire:target='update'>
                            <option value="" selected>Pilih status pembayaran</option>
                            <option value="success">Succes</option>
                            <option value="pending">Pending</option>
                        </select>
                        @error('status')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>
                    @endif
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    @if ($edit == true)
                    <button wire:click='update' type="button" class="btn btn-warning" wire:loading.attr="disabled"
                        wire:target='update'>
                        <span wire:loading.block wire:target='update'>
                            <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                            <span>Loading...</span>
                        </span>
                        <span wire:loading.remove wire:target='update'>Simpan Perubahan</span>
                    </button>
                    @else
                    <button wire:click='store' type="button" class="btn btn-primary" wire:loading.attr="disabled"
                        wire:target='store'>
                        <span wire:loading.block wire:target='store'>
                            <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                            <span>Loading...</span>
                        </span>
                        <span wire:loading.remove wire:target='store'>Simpan Data</span>
                    </button>
                    @endif
                </div>
            </div>
        </div>
    </div>


    <script>
        document.addEventListener('deleteConfrimed', function() {
            Swal.fire({
                    title: "Hapus?",
                    text: "Apa Kamu yakin menghapus berita ini?",
                    icon: "warning",
                    showCancelButton: true,
                    confirmButtonText: 'Yes, delete!',
                    cancelButtonText: 'Tidak',
                })
                .then((next) => {
                    if (next.isConfirmed) {
                        Livewire.emit('deleteAction');
                    } else {
                        Swal.fire("Your news is save!");
                    }
                });
        })

        window.addEventListener('showModal', event => {
           $('#NewModal').modal('show');
        })
        window.addEventListener('expandModal', event => {
           $('#NewModal').modal('hide');
        })

        window.addEventListener('success', event => {
            Swal.fire({
                icon: 'success',
                title: 'Good Jobs!',
                text: event.detail,
            })
        })
        window.addEventListener('erros', event => {
            Swal.fire({
                icon: 'error',
                title: 'Opps...!',
                text: event.detail,
            })
        })
    </script>

</div>