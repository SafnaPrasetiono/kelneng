<div>
    <div class="d-flex flex-column flex-sm-row align-items-sm-center rounded bg-white shadow-sm p-3 mb-3">
        <div class="lh-sm">
            <p class="fs-4 fw-bold mb-0">Kas keluarga</p>
        <p class="mb-0 fw-light">Halaman dana kas keluarga nengsih</p>
        </div>
        <div class="d-none d-sm-block ms-sm-auto lh-1 text-end pe-4 ps-5 py-2 border rounded">
            <h4 class="fw-bold mb-0">Rp. {{ number_format($price, 0, ',', '.') }}</h4>
            <small class="mb-0 fw-light text-danger">+ Rp. {{ number_format($pending, 0, ',', '.') }}</small>
        </div>
    </div>
    <div class="d-flex d-sm-none align-items-center rounded bg-white shadow-sm p-3 mb-3">
        <i class="fas fa-sack-dollar fa-2x"></i>
        <div class="ms-auto lh-1 text-end">
            <h4 class="fw-bold mb-0">Rp. {{ number_format($price, 0, ',', '.') }}</h4>
            <small class="mb-0 fw-light text-danger">+ Rp. {{ number_format($pending, 0, ',', '.') }}</small>
        </div>
    </div>

    <div class="row g-3">
        <div class="col-6 col-sm-6 col-md-6 col-lg-6">
            <div class="d-block bg-white rounded-3 shadow-sm mb-3">
                <div class="p-3 text-end border-bottom">
                    <h4 class="fw-light mb-0">Rp. {{ number_format($tf, 0, ',', '.') }}</h4>
                </div>
                <div class="px-3 py-2">
                    <p class="mb-0 fw-bold text-secondary">Transfer</p>
                </div>
            </div>
        </div>
        <div class="col-6 col-sm-6 col-md-6 col-lg-6">
            <div class="d-block bg-white rounded-3 shadow-sm mb-3">
                <div class="p-3 text-end border-bottom">
                    <h4 class="fw-light mb-0">Rp. {{ number_format($case, 0, ',', '.') }}</h4>
                </div>
                <div class="px-3 py-2">
                    <p class="mb-0 fw-bold text-secondary">Kes</p>
                </div>
            </div>
        </div>
    </div>
</div>