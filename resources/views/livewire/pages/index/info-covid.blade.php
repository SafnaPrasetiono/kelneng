<div>
    <div class="py-5 alert alert-secondary p-0 m-0 rounded-0 border-0">
        <div class="container">
            <div class="row g-3">
                <div class="col-12">
                   <div class="mb-3">
                    <h3 class="fw-bold">Info Covid-19</h3>
                    <p class="mb-0">Data berdasarkan dari covid-19 pemerintahaan</p>
                   </div>
                </div>
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="d-block rounded border bg-white p-3 w-100">
                        <div class="d-flex align-items-center justify-content-between py-3">
                            <i class="fas fa-users fa-5x"></i>
                            <p id="confrimed" class="display-5"></p>
                        </div>
                        <div class="pt-2 border-top fw-bold">Terkonfirmasi</div>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="d-block rounded border bg-white p-3 w-100">
                        <div class="d-flex align-items-center justify-content-between py-3">
                            <i class="fas fa-skull-crossbones fa-5x"></i>
                            <p id="death" class="display-5"></p>
                        </div>
                        <div class="pt-2 border-top fw-bold">Meninggal</div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="{{ url('/assets/dist/js/main/jquery.js') }}"></script>
    <script src="{{ url('/assets/dist/js/main/autoNumeric.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>

<script>
    function formatRupiah(bilangan){
        var	number_string = bilangan.toString(),
        sisa 	= number_string.length % 3,
        rupiah 	= number_string.substr(0, sisa),
        ribuan 	= number_string.substr(sisa).match(/\d{3}/g);
            
        if (ribuan) {
            separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
        }
		return rupiah;
	}

    axios.get('https://covid19.mathdro.id/api/countries/ID/confirmed')
    .then(function (response) {
        // handle success
        // console.log(response.data);
        $('#confrimed').html(formatRupiah(response.data[0].confirmed));
        $('#death').html(formatRupiah(response.data[0].deaths));
    })
    .catch(function (error) {
        // handle error
        console.log(error);
    })
    .then(function () {
        // always executed
    });

</script>
</div>
