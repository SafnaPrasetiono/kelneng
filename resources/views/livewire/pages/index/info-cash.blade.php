<div>
    <div class="alert alert-secondary m-0 p-0 rounded-0 border-0 py-5">

        <div class="container">
            <div class="d-block mb-3">
                <h2 class="fw-bold">Info Kas Keluarga</h2>
                <p class="mb-0">Detail informasi terkait kas keluarga nengsih</p>
            </div>
            <div class="row">
                <div class="col-12 col-md-4">
                    <div class="d-block rounded bg-white shadow-sm">
                        <div class="d-flex justify-content-between align-items-center p-3 border-bottom">
                            <i class="fas fa-dollar-sign fa-2x fa-fw"></i>
                            <span class="fw-light fs-2">{{ number_format($cash,0,',','.') }}</span>
                        </div>
                        <p class="mb-0 px-3 py-2 fw-bold">Total Kas</p>
                    </div>
                </div>
                <div class="col-12 col-md-4">
                    <div class="d-block rounded bg-white shadow-sm">
                        <div class="d-flex justify-content-between align-items-center p-3 border-bottom">
                            <i class="fas fa-history fa-2x fa-fw"></i>
                            <span class="fw-light fs-2">{{ number_format($cashPending,0,',','.') }}</span>
                        </div>
                        <p class="mb-0 px-3 py-2 fw-bold">Kas Tertunda</p>
                    </div>
                </div>
                <div class="col-12 col-md-4">
                    <div class="d-block rounded bg-white shadow-sm">
                        <div class="d-flex justify-content-between align-items-center p-3 border-bottom">
                            <i class="fas fa-users fa-2x fa-fw"></i>
                            <span class="fw-light fs-2">{{ $buyerCount }}</span>
                        </div>
                        <p class="mb-0 px-3 py-2 fw-bold">User Bayar</p>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>