<div>
    {{-- this cash html --}}
    <div class="py-5">
        <div class="container">
            <div class="d-block mb-3">
                <h3 class="mb-0">Data Uang Kas</h3>
                <p class="mb-0">Pemasukan Uang kas keluarga nengsih</p>
            </div>
            <div class="d-block mb-3">
                <div class="d-flex">
                    <div class="d-flex me-2">
                        <?php $monthArray = ['JANUARI','FEBRUARI','MARET','APRIL','MEI','JUNI','JULI','AGUSTUS','SEPTEMBER','OKTOBER','NOVEMBER','DESEMBER'];?>
                        <select wire:model='month' class="form-select" aria-label="Default select example">
                            <option selected>Pilih Bulan</option>
                            @foreach ($monthArray as $index => $item)
                            <option value="{{ $index + 1 }}">{{ $item }}</option>
                            @endforeach
                        </select>
                        <div style="width: 150px">
                            <?php $date = date('Y'); ?>
                            <select wire:model='years' name="years" id="years" class="form-select ms-1">
                                @for ($i = $date; $i > 2020; $i--)
                                <option value="{{ $i }}">{{ $i }}</option>
                                @endfor
                            </select>
                        </div>
                    </div>
                    <div class="ms-auto position-relative">
                        <input wire:model='name' type="text" class="form-control me-5" placeholder="Cari Nama...">
                        <button class="btn position-absolute top-0 end-0 border-0">
                            <i class="fas fa-search fa-sm fa-fw"></i>
                        </button>
                    </div>
                </div>
            </div>
            <div class="position-relative">
                @if($data->count() != 0)
                <div class="d-block table-responsive">
                    <table class="table table-borderless">
                        <thead class="alert alert-secondary p-0 m-0">
                            <tr>
                                <th>No</th>
                                <th>Nama</th>
                                <th>Pembayaran</th>
                                <th>Total</th>
                                <th>Status</th>
                                <th>Tanggal</th>
                                <th>Bukti</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data as $index => $item)
                            <tr>
                                <td scope="row">{{ $index + 1 }}</td>
                                <td>{{ $item->username }}</td>
                                <td>{{ $item->method }}</td>
                                <td>Rp. {{ number_format($item->price,0,',','.') }}</td>
                                <td>
                                    @if($item->status == "success")
                                    <span class="badge text-bg-success rounded-pill px-3">success</span>
                                    @else
                                    <span class="badge text-bg-secondary rounded-pill px-3">pending</span>
                                    @endif
                                </td>
                                <td>{{ date('d F Y', strtotime($item->date)) }}</td>
                                <td>
                                    <button wire:click='show({{ $item->id_cash }})' class="btn btn-outline-primary btn-sm">
                                        <i class="fas fa-eye fa-sm fa-fw"></i>
                                    </button>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                @else
                <div class="d-block">
                    <div class="alert alert-info p-5 text-center">
                        <i class="fas fa-money-bill-alt fa-5x fa-fw mb-4"></i>
                        <h2 class="mb-0 fw-bold">OOPS!</h2>
                        <p class="mb-0 fw-bold">Data kas bulan ini belum ada</p>
                    </div>
                </div>
                @endif
                <div wire:loading.flex wire:target='month, years, name' class="justify-content-center align-items-center position-absolute top-0 h-100 w-100" style="background-color: #eaeaea75">
                    <div class="spinner-border" role="status">
                      <span class="visually-hidden">Loading...</span>
                    </div>
                  </div>
            </div>
        </div>
    </div>

    @if($images)
    <div class="modal fade" id="cashModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Bukti Pembayaran</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body p-0">
                    <img src="{{ url('/images/cash/' . $images) }}" alt="{{ $images }}" class="img-fluid">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary px-5" data-bs-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    @endif
    
    <script>
        window.addEventListener('DOMContentLoaded', () => {
            window.addEventListener('showModals', () => {
                console.log('this open');
                $('#cashModal').modal('show');
            })
            
        });
    </script>
</div>