@extends('admin.layouts.panel')

@section('head')
    <title>wellcome in dashbaord kopitu preneur</title>
@endsection

@section('pages')
    <div class="container-fluid">
        <div class="d-block rounded-3 bg-white shadow-sm p-3 mb-3">
            <h4 class="fw-bold mb-0">Dashboard</h4>
            <p class="fw-light mb-0">Wellcome in dashboard</p>
        </div>

        <div class="d-block mb-3">
            <div class="row g-3">
                <?php for($x=1; $x<=4; $x++) : ?>
                <div class="col-12 col-sm-6 col-md-4 col-lg-3">
                    <div class="d-block rounded-3 bg-white shadow-sm">
                        <div class="d-flex align-items-center p-3 border-bottom">
                            <i class="fas fa-users fa-3x fa-fw text-secondary"></i>
                            <span class="fs-1 ms-auto">100</span>
                        </div>
                        <div class="py-2 px-3">
                            Data Admin
                        </div>
                    </div>
                </div>
                <?php endfor; ?>
            </div>
        </div>
    </div>
@endsection

@section('script')
    
@endsection