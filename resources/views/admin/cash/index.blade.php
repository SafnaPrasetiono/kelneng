@extends('admin.layouts.panel')

@section('head')
<title>Kelneng - Data Kas Keluarga</title>
@endsection

@section('pages')
<div class="container-fluid">
    @livewire('admin.cash.counter')
    <div class="d-block rounded bg-white shadow-sm mb-3">
       @livewire('admin.cash.data')
    </div>
</div>
@endsection

@section('script')

@endsection