@extends('pages.layouts.panel')

@section('head')
<title>Selamat datang di keluarga nengsih</title>
<style>
    .img-banners {
        width: 100%;
        height: 100%;
        background-position: center;
        background-size: cover;
        background-repeat: no-repeat;
    }

    .btn-images-news {
        width: 100%;
        height: 100%;
        background-position: center;
        background-size: cover;
        background-repeat: no-repeat;
    }
</style>
@endsection

@section('pages')
<div class="d-block" style="height: 60px"></div>
@livewire('pages.index.banners')
@livewire('pages.index.info-cash')
@livewire('pages.index.cash')
@livewire('pages.index.news')
@endsection

@section('script')

@endsection