<!DOCTYPE html>
<html lang="id">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <meta name="author" content="safna prasetiono">
    <meta name="description"
        content="Keluarga nengsih merupakan website profile dari keluarga besar nengsih">
    <meta name="keywords"
        content="kelneng, keluarga nengsih, nengsih">
    <link rel="alternate" type="application/rss+xml" title="Selamat data di keluar besar nengsih">

    <meta name="msapplication-navbutton-color" content="#ffffff" />
    <meta name="apple-mobile-web-app-status-bar-style" content="#ffffff" />
    {{-- <link rel="icon" type="image/png" href="{{asset('/images/logo/kopitu-estore.png')}}" /> --}}

    @yield('head')
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="{{ url('/assets/app/css/app.css') }}">
    <link rel="stylesheet" href="{{ url('/assets/icons/css/all.css') }}">
    <link rel="stylesheet" href="{{ url('/assets/dist/css/main/index.css') }}">
    <link rel="stylesheet" href="{{ url('/assets/dist/css/main/animated.css') }}">
    <link rel="stylesheet" href="{{ asset('/assets/splide/css/splide.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/assets/owl/css/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/assets/owl/css/owl.theme.default.min.css') }}">
    @livewireStyles
</head>

<body>

    <nav class="navbar navbar-expand-lg navbar-light fixed-top bg-white">
        <div class="container">
            <a class="navbar-brand fw-bold" href="{{ route('index') }}">
                KEL<span class="text-primary">NENG</span>
            </a>
            <button class="btn px-3 d-inline d-lg-none" type="button" data-bs-toggle="collapse"
                data-bs-target="#navbarSlider">
                <i class="fas fa-bars fa-sm fa-fw text-secondary"></i>
            </button>
            <div class="collapse navbar-collapse" id="navbarSlider">
                <ul class="navbar-nav ms-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('index') }}">Beranda</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Kegiatan</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Keanggotaan</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Galeri</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('about') }}">Tentang Kami</a>
                    </li>
                    @auth('user')
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                            data-bs-toggle="dropdown" aria-expanded="false">
                            <img class="rounded-circle"
                                src="{{ url('/images/avatar/user/' . auth('user')->user()->avatar) }}" alt="user"
                                width="28px" height="28px">
                            <span class="text-capitalize">
                                {{ auth('user')->user()->username }}
                            </span>
                        </a>
                        <ul class="dropdown-menu border-0 shadow-lg" aria-labelledby="navbarDropdown">
                            <li><a class="dropdown-item" href="{{ route('user.profile') }}">Profile</a></li>
                            <li><a class="dropdown-item" href="{{ route('user.lamaran') }}">Lamaran Saya</a></li>
                            <li><a class="dropdown-item" href="{{ route('user.setting') }}">Peraturan</a></li>
                            <li>
                                <hr class="dropdown-divider">
                            </li>
                            <li><a class="btnLogout dropdown-item" href="#">Logout</a></li>
                        </ul>
                    </li>
                    @else
                    <li class="nav-item d-inline d-lg-none">
                        <a class="nav-link link-primary" href="#">Daftar</a>
                    </li>
                    <li class="nav-item d-inline d-lg-none">
                        <a class="nav-link link-primary" href="#">Login</a>
                    </li>
                    <li class="nav-item">
                        <a href="#"
                            class="btn btn-outline-primary d-none d-lg-inline-block px-4 mx-2">Daftar</a>
                    </li>
                    <li class="nav-item">
                        <a href="#" class="btn btn-primary d-none d-lg-inline-block px-4">Masuk</a>
                    </li>
                    @endauth
                </ul>
            </div>
        </div>
    </nav>

    <section>
        @yield('pages')
    </section>

    <footer>
        <div class="py-5 bg-primary">
            <div class="container">
                <div class="row gy-4">
                    <div class="col-12 col-md-8">
                        <div class="text-white mb-4">
                            <span class="fw-bold fs-5 fw-bold">Info Website</span>
                            <hr class="soft" width="50px">
                            <div class="pe-0 pe-md-5">
                                <p>Website ini merupakan resmi web milik keluarga besar nengsih yang dipergunakan sebagai dokumentasi dari kegiatan dari keluar besar nengsih. Lebih detail lihat pada menu tentang kami mengenai website keluarga nengsih.</p>
                                <div class="d-flex mb-2">
                                    <i class="fas fa-phone fa-fw"></i>
                                    <p class="mb-0 ms-2">+62 877-7833-5325</p>
                                </div>
                                <div class="d-flex mb-2">
                                    <i class="fas fa-envelope fa-fw"></i>
                                    <p class="mb-0 ms-2">info@kelneng.site</p>
                                </div>
                                <div class="d-flex mb-2">
                                    <i class="fas fa-map-marked fa-fw"></i>
                                    <p class="mb-0 ms-2">Jl. Cemp. Putih Bar. No.3, RT.9/RW.13, Cemp. Putih Bar., Kec. Cemp. Putih, Kota Jakarta Pusat, Daerah Khusus Ibukota Jakarta 10520</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-4">
                        <div class="text-white">
                            <span class="fw-bold fs-5">Link Terkait</span>
                            <hr class="soft" width="50px">
                            <nav class="nav flex-column">
                                <a class="nav-link link-light p-0 mb-3" href="#">
                                    Cara Mendaftar
                                </a>
                                <a class="nav-link link-light p-0 mb-3" href="#">
                                    Syarat Ketentuan
                                </a>
                                <a class="nav-link link-light p-0 mb-3" href="#">
                                    Kebijakan Privasi
                                </a>
                                <a class="nav-link link-light p-0 mb-3" href="#">Tentang Kami</a>
                            </nav>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="text-white">
                            <span class="fw-bold fs-5 fw-bold">Social Media</span>
                            <hr class="soft" width="50px">
                            <nav class="nav">
                                <a class="nav-link py-0 px-0 me-3" target="balnk" href="#">
                                    <img src="{{ url('/images/icons/facebook.png') }}" alt="facebook"
                                        class="rounded-circle" width="42px" height="42px">
                                </a>
                                <a class="nav-link py-0 px-0 me-3" target="balnk" href="#">
                                    <img src="{{ url('/images/icons/twitter.png') }}" alt="twitter"
                                        class="rounded-circle" width="42px" height="42px">
                                </a>
                                <a class="nav-link py-0 px-0 me-3" target="balnk" href="#">
                                    <img src="{{ url('/images/icons/instagram.png') }}" alt="instagram"
                                        class="rounded-circle" width="42px" height="42px">
                                </a>
                                <a class="nav-link py-0 px-0 me-3" target="balnk" href="#">
                                    <img src="{{ url('/images/icons/tiktok.png') }}" alt="facebook"
                                        class="rounded-circle" width="42px" height="42px">
                                </a>
                                <a class="nav-link py-0 px-0 me-3" target="balnk" href="#">
                                    <img src="{{ url('/images/icons/youtube.png') }}" alt="youtube"
                                        class="rounded-circle" width="42px" height="42px">
                                </a>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="py-2" style="background-color: #0d5ed6">
            <div class="container">
                <div class="text-center text-white">
                    <small>Suported by komite pengusaha mikro kecil menengah indonesia bersatu (KOPITU)</small>
                </div>
            </div>
        </div>
    </footer>

    <script src="{{ url('/assets/dist/js/main/jquery.js') }}"></script>
    <script src="{{ url('/assets/dist/js/main/popper.js') }}"></script>
    <script src="{{ url('/assets/app/js/app.js') }}"></script>
    <script src="{{ url('/assets/dist/js/main/alert.js') }}"></script>
    <script src="{{ url('/assets/dist/js/main/index.js') }}"></script>
    <script src="{{ asset('/assets/splide/js/splide.min.js') }}"></script>
    <script src="{{ asset('/assets/owl/owl.carousel.min.js') }}"></script>
    @livewireScripts
    @yield('script')

    @if(session()->has('success'))
    <script>
        Swal.fire({
            icon: 'success',
            title: 'Good Jobs!',
            text: '{{ session()->get("success") }}',
            showConfirmButton: false,
            timer: 2500
        })
    </script>
    @elseif(session()->has('error'))
    <script>
        Swal.fire({
            icon: 'error',
            title: 'Opps...!',
            text: '{{ session()->get("error") }}',
            showConfirmButton: false,
            timer: 2500
        })
    </script>
    @endif
</body>

</html>