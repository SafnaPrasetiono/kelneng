<!DOCTYPE html>
<html lang="id">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <meta name="author" content="safna prasetiono">
    <meta name="description" content="Keluarga nengsih merupakan website profile dari keluarga besar nengsih">
    <meta name="keywords" content="kelneng, keluarga nengsih, nengsih">
    <link rel="alternate" type="application/rss+xml" title="Selamat data di keluar besar nengsih">

    <meta name="msapplication-navbutton-color" content="#ffffff" />
    <meta name="apple-mobile-web-app-status-bar-style" content="#ffffff" />
    {{--
    <link rel="icon" type="image/png" href="{{asset('/images/logo/kopitu-estore.png')}}" /> --}}

    @yield('head')
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="{{ url('/assets/app/css/app.css') }}">
    <link rel="stylesheet" href="{{ url('/assets/icons/css/all.css') }}">
    <link rel="stylesheet" href="{{ url('/assets/dist/css/auth/auth.css') }}">
    <link rel="stylesheet" href="{{ url('/assets/dist/css/main/animated.css') }}">
    <link rel="stylesheet" href="{{ asset('/assets/owl/css/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/assets/owl/css/owl.theme.default.min.css') }}">
    @livewireStyles
</head>

<body>

    @yield('pages')

    <script src="{{ url('/assets/dist/js/main/jquery.js') }}"></script>
    <script src="{{ url('/assets/dist/js/main/popper.js') }}"></script>
    <script src="{{ url('/assets/app/js/app.js') }}"></script>
    <script src="{{ url('/assets/dist/js/main/alert.js') }}"></script>
    <script src="{{ asset('/assets/owl/owl.carousel.min.js') }}"></script>
    @livewireScripts
    @yield('script')

    @if(session()->has('success'))
    <script>
        Swal.fire({
            icon: 'success',
            title: 'Good Jobs!',
            text: '{{ session()->get("success") }}',
            showConfirmButton: false,
            timer: 2500
        })
    </script>
    @elseif(session()->has('error'))
    <script>
        Swal.fire({
            icon: 'error',
            title: 'Opps...!',
            text: '{{ session()->get("error") }}',
            showConfirmButton: false,
            timer: 2500
        })
    </script>
    @endif
</body>

</html>