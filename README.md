<h3>KELNENG PROJECT</h3>

## Tentang Kelneng

Aplikasi ini dubuat untuk website keluarga nengsih yang bertujuan untuk mengetahui informasi secara transparan baik data kegiatan keluarnengsih seperti dana keuangan yang ditujukan untuk para keluarga nengsih. Web ini masih dalam masa pengembangan yang dimana masih terdapat update dan bug yang tidak terduga.

## Pembuatan Website Kelneng

- **[LARAVEL](https://laravel.com/)**
- **[BOOTSTRAP](https://getbootstrap.com)**
- **[FONT-AWESOME](https://fontawesome.com)**
- **[JQuery](https://jquery.com)**

## Contributing

Terimakasi atas suportnya sehingga terciptanya project web kelneng ini yang dimana tentu untuk mensuport keluarga nengsih untuk tetap berkarya.
## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
